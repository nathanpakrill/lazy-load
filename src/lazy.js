var lazy = (function() {
    'use strict';

    var images,
        lazyImages = [];

    function load(els) {
        els.forEach(function (e) {
            e.src = e.dataset.src;
        });
    }

    function init () {
        images = document.querySelectorAll('img');
        images.forEach(function (i) {
            if (i.dataset.src) {
                lazyImages.push(i);
            }
        });

        load(lazyImages);
    }

    return {
        init: init
    }
}());
