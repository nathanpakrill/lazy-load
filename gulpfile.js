var gulp = require('gulp'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename');

gulp.task('default', function () {
    return gulp.src([
            'node_modules/@fdaciuk/ajax/dist/ajax.min.js',
            'src/lazy.js'
        ])
        .pipe(uglify({
            keep_fnames: true
        }))
        .pipe(rename('lazy.min.js'))
        .pipe(gulp.dest('dist'));
});
