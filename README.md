# Lazy Load

Lazy Load images to reduce initial load times

## Installation
- Clone the repo
- Include `dist/lazy.min.js` in your project

## Usage
- Create `<img>` elements with a `data-src` attribute that points to the image location.
- In your javascript, call `lazy.init()` to run the load.

- To have an image in place (perhaps a low-res one) before the load, use the `src` attribute of you `<img>` with the path to the placeholder image.

Example:

```
    <img src='/path/to/placeholder' data-src='/path/to/image'>
    <script type="text/javascript">
        lazy.init();
    </script>
```

## Contribution
- Clone the repo
- Edit `src/lazy.js`
- Run `npm install` in the CLI
- Run `gulp` in the CLI
- Push changes
